import socketio
import cv2
from src.tools import encode_img_base64

import time

cap = cv2.VideoCapture(0) 

while 1:

    ok, frame = cap.read()
 
    if not ok:
        break
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    print(cv2.Laplacian(gray, cv2.CV_64F).var())

    cv2.imshow("Feed", frame)

    key = cv2.waitKey(5) & 0xFF

