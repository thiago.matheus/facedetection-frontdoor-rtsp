"""
Utility functions for socket server and client
"""

import numpy as np
import math
import cv2
from io import BytesIO
from PIL import Image
import base64
#from numba import jit

def find_closest(emb, database):
    dists = []
    for emb_test in database["encodings"]:
        emb_test = emb_test.reshape(1, emb_test.shape[0])
        dists.append(emb_similarity(emb, emb_test))
    return dists

#@jit(nopython=True)
def emb_similarity(embeddings1, embeddings2):
    """Return the distance of two vectors

    Arguments
    ---------
    embeddings1: numpy.ndarray
        512D vector for comparison
    embeddings2: numpy.ndarray
        512D vector for comparison
    distance_metric: int
        Which method for calculating distance 0 for euclidean distance 1 for cosine similarity

    Returns
    -------
    dist: float
        The distance/similarity between the two given vectors
    """
    # if distance_metric==0:
    #     # Euclidian distance
    #     diff = np.subtract(embeddings1, embeddings2)
    #     dist = np.sum(np.square(diff),1)
    # elif distance_metric==1:
    #     # Distance based on cosine similarity
    #     # dot = np.sum(np.multiply(embeddings1, embeddings2), axis=1)
    dot = np.dot(embeddings1[0], embeddings2[0])
    norm = np.linalg.norm(embeddings1[0]) * np.linalg.norm(embeddings2[0])
    similarity = dot / norm
    return similarity

def cosine_similarity(embeddings1, embeddings2):
    # Distance based on cosine similarity
    dot = np.sum(np.multiply(embeddings1, embeddings2), axis=1)
    norm = np.linalg.norm(embeddings1, axis=1) * np.linalg.norm(embeddings2, axis=1)
    similarity = dot / norm
    dist = np.arccos(similarity) / math.pi

    return dist
#Distance between 2 points
def distance(p1,p2):
    return math.hypot(p1[0] - p2[0], p1[1]-p2[1])

#Calculates angle between eyes using simple trigonometry
def eyes_angle(eyes_dist, l_eye, r_eye):
    a = l_eye[0] - r_eye[0]
    return math.degrees(math.acos(a/eyes_dist))


#Buggy and ugly function this needs a refactoring
def is_frontal_face(bbs, point,frame, draw_points=False):
    #TODO Refactoring
    try:
        rec = "Not aligned"
        r_eye = (int(point[0]), int(point[5]))
        l_eye = (int(point[1]), int(point[6]))
        nose = (int(point[2]), int(point[7]))
        r_mouth = (int(point[3]), int(point[8]))
        l_mouth = (int(point[4]), int(point[9]))

        r_eye_nose_dist = distance(r_eye, nose)
        l_eye_nose_dist = distance(l_eye, nose)
        r_m_nose_dist = distance(r_mouth, nose)
        l_m_nose_dist = distance(l_mouth, nose)

        eyes_dist = distance(r_eye, l_eye)

        eyes_dist_avg = (r_eye_nose_dist+l_eye_nose_dist)/2
        mouth_dist_avg = (r_m_nose_dist+l_m_nose_dist)/2

        if eyes_dist < 25:
            print("small face")
            cv2.putText(frame, "Move closer to the camera", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
            rec = 'Small face'
        elif eyes_angle(eyes_dist, l_eye, r_eye) > 10:
            rec = 'Rotated face'
            cv2.putText(frame, "Your face is rotated!", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
        elif r_eye_nose_dist/l_eye_nose_dist < 0.85:
            cv2.putText(frame, "Please look to the left!", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
            rec = 'Looking right'
        elif l_eye_nose_dist/r_eye_nose_dist < 0.85:
            cv2.putText(frame, "Please look to the right!", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
            rec = 'Looking left'
        elif eyes_dist_avg/mouth_dist_avg < 0.70:
            cv2.putText(frame, "Please look down!", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
            rec = "Looking up"
        elif mouth_dist_avg/eyes_dist_avg < 0.80:
            cv2.putText(frame, "Please look up!", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
            rec = "Looking down"
        else:
            cv2.putText(frame, "Perfect!", (0, 50),cv2.FONT_HERSHEY_SIMPLEX,1, (0,0,255), 3)
            rec = "Aligned"

        if draw_points and rec:
            cv2.circle(frame, r_eye, 5 , (255,255,255)) # Right eye
            cv2.circle(frame, nose, 5 , (255,255,255)) # Nose
            cv2.circle(frame, r_mouth ,5 , (255,255,255)) # Mouth right
            cv2.circle(frame, l_eye, 5 , (255,255,255)) # Left eye
            cv2.circle(frame, l_mouth ,5 , (255,255,255)) # Left mouth

    except Exception as e:
        pass
    return rec

def decode_img_base64(base64_string):
    sbuf = BytesIO()
    sbuf.write(base64.b64decode(base64_string))
    pimg = Image.open(sbuf)
    return cv2.cvtColor(np.array(pimg), cv2.COLOR_RGB2BGR)

def encode_img_base64(img):
    _, img = cv2.imencode('.jpg', img)
    img_encoded = base64.b64encode(img).decode('utf-8')
    return img_encoded
