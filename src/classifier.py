from sklearn.neural_network import MLPClassifier
from dataset import Dataset
from sklearn.externals import joblib
import numpy as np
import pickle
import cv2
import os

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier

from progress.bar import Bar

class Classifier:

    def __init__(self, classifier_path="classifiers/classifier.pkl", label_map="classifiers/classifier_label_map.pkl"):
        """Classifier for identifying face

        Arguments
        ---------
        classifier_path: str
            Path to scikit learn pickled classifier
        label_map: str
            Path to a pickled list corresponding to the labels of the classifier
        """
        self.classifier_path = classifier_path
        self.label_map_path = label_map
        self.classifier = joblib.load(classifier_path)

        with open(label_map, 'rb') as f:
            self.label_map = pickle.load(f)

    def classify(self, emb):
        """Classify embedding

        Arguments
        ---------
        emb: numpy.ndarray
            512d array to classify

        Returns
        -------
        results: list
            List containing probabilities for each class
        """

        results = self.classifier.predict_proba(emb)[0]
        return results

    def get_best(self, results):
        """Get label of class that got highest probability

        Arguments
        ---------
        results: numpy.ndarray
            512d array to classify

        Returns
        -------
        name: str
            Label of highest probability class
        score: float
            Highest probability
        """

        best = np.argmax(results)

        name = self.label_map[best]
        score = results[best]

        return name, score

    def train_classifier(self, dataset, facenet,classifier_name, save_path='classifiers', model="mlp", verbose=0):
        """Train sklearn face classifier

        Arguments
        ---------
        dataset: Dataset
            The dataset to train on
        facenet: FaceNet
            A facenet model class instance
        classifier_name: str
            The prefix name of classifier and label mapping
        save_path: str
            Where to save the trained classifier
        model: str
            Which sklearn model to use
        """
        if model == "mlp":
            model=MLPClassifier(hidden_layer_sizes=(1500,),verbose=verbose)
        elif model == "svm":
            model = SVC(C=5, kernel='linear', probability=True, verbose=verbose)    
        elif model == "naive":
            model = GaussianNB()    
        elif model == "knn":
            model = KNeighborsClassifier(n_neighbors=10, metric='euclidean')

        train_data, test_data = self.generate_embedding_data(dataset, facenet)
        train_labels = dataset.labels_flat_train
        test_labels = dataset.labels_flat_test

        model.fit(train_data, train_labels)

        y_pred = model.predict(test_data)
        
        print(classification_report(test_labels, y_pred))
        print(confusion_matrix(test_labels, y_pred))
        print('accuracy is',accuracy_score(y_pred,test_labels))

        joblib.dump(model, "{}.pkl".format(os.path.join(save_path,classifier_name)), compress=9)
        f = open("{}.pkl".format(os.path.join(save_path, "{}_label_map".format(classifier_name))), 'wb')
        pickle.dump(dataset.get_classes(), f)
        f.close()

    def generate_embedding_data(self, dataset, facenet, embedding_size=512):
        """Generates face embeddings for training classifier

        Arguments
        ---------
        dataset: Dataset
            The dataset to train on
        facenet: FaceNet
            A facenet model class instance
        embedding_size: int
            The dimension size of facenet encoding

        Returns
        -------
        train_embedding_data: numpy.ndarray
            Numpy array containing face encodings for training
        test_embedding_data: numpy.ndarray
            Numpy array containing face encodings for testing
        """

        train_paths = dataset.image_paths_train
        test_paths = dataset.image_paths_test

        train_embedding_data = np.zeros((len(train_paths), embedding_size))
        test_embedding_data = np.zeros((len(test_paths), embedding_size))
        
        bar = Bar('Processing training data', max=len(train_paths))
        for i, img in enumerate(train_paths):
            train_embedding_data[i] = facenet.run_single(cv2.imread(img))[0]
            bar.next()
        bar.finish()

        bar = Bar('Processing testing data', max=len(test_paths))
        for i, img in enumerate(test_paths):
            test_embedding_data[i] = facenet.run_single(cv2.imread(img))[0]
            bar.next()
        bar.finish()

        return train_embedding_data, test_embedding_data

    def remove_person(self, name, dataset, facenet, classifier_name, save_path='classifiers'):
        """Removes person from dataset and retrains model"""

        dataset.remove_person(name)

        #TODO make assert person exists

        self.train_classifier(dataset, facenet, classifier_name, save_path=save_path)
