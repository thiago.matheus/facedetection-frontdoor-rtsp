from threading import Thread
import socketio
import cv2
from src.tools import encode_img_base64
import sys
import  json
import requests
import time
import numpy as np
import dlib

class VideoProcessConnect:
	"""
	Class that continuously shows a frame using a dedicated thread.
	"""

	def __init__(self, frame=None):
		self.frame = frame
		self.stopped = False

		self.net = dnnFaceDetector = dlib.cnn_face_detection_model_v1("mmod_human_face_detector.dat")
		
		self.recognized_name = ""
		self.last_time_detected = time.time()

		self.tempo_de_inferencia_inicial = time.time()


	def start(self):
		Thread(target=self.show, args=()).start()
		return self

	# Função que roda dentro da thread em um loop infinito
	def show(self):
		
		self.last_time_detected = time.time()

		global last_detected
		last_detected = ""

		sio = socketio.Client()

		self.sio= sio

		#Actions
		def opendoor(prediction=''):
			try:
			   # _resp = requests.get("http://aia-webhook.ngrok.io/opendoor?name={}".format(prediction))
				_resp=requests.get("http://192.168.1.160:5000/opendoor")

				print("Resposta request {}".format(_resp))
			except IOError:
				_type, value, _traceback = sys.exc_info()
			except Exception as e:
				print(e)


		def playaction(prediction=''):
			headers = {
				"Content-Type": "application/json"
			}
			data = json.dumps({
				"name": prediction
			})
			print(headers,data)
			#r = requests.post("http://localhost:7000/projector", headers=headers, data=data)
			r=requests.post("https://keyapp.ngrok.io/projector",headers=headers, data=data)
			print(r)
			if r.status_code != 200:
				print(r.status_code)
				print(r.text)


		@sio.on('connect')
		def on_connect():
			print('[INFO] Connection successfull')

			config = {
				'recognition_threshold' : 0.7,
				'laplacian_threshold' : 80
			}

			sio.emit('config', config)

		@sio.on('connected')
		def on_resonse(sid):

			data = {
				'frame' : encode_img_base64(self.frame)
			}

			if ok:
				sio.emit('recognize', data)

		@sio.on('identified')
		def identified(data):
			global last_detected

			identified = data['name']
			orientation = data['orientation']

			# if orientation != 'Aligned':
			# 	print(orientation)
			if identified == 'unknown':
				self.recognized_name = ""
				pass
			elif( identified == "blurry"):
				self.recognized_name = ""
				pass
			elif( identified == ""):
				self.recognized_name = ""
				pass
			else:
				recognized_name = identified.upper()
				if last_detected==recognized_name and time.time() - self.last_time_detected < 10:
					print("Mesma pessoa. Ainda não passou 10 segundos.")
					pass
				elif (last_detected==(recognized_name) and time.time() - self.last_time_detected > 10 and recognized_name != ""):
					last_detected=recognized_name
					self.last_time_detected = time.time()
					opendoor(recognized_name)
					playaction(prediction=recognized_name)
					print("Nome: {}".format(recognized_name))
					print("Mesma pessoa 10 seg depois")
					self.recognized_name = recognized_name
					# Calcula o tempo de inferencia
					tempo_de_inferencia_inicial = self.tempo_de_inferencia_inicial
					tempo_de_inferencia_final = time.time()
					tempo_de_inferencia = tempo_de_inferencia_final - tempo_de_inferencia_inicial
					print("Tempo de inferencia: {0:.5f}".format(tempo_de_inferencia))


				elif last_detected != recognized_name and recognized_name != "":
					last_detected=recognized_name
					self.last_time_detected = time.time()
					opendoor(recognized_name)
					playaction(prediction=recognized_name)
					print("Nome: {}".format(recognized_name))
					print("Pessoas diferentes")
					self.recognized_name = recognized_name
					# Calcula o tempo de inferencia
					tempo_de_inferencia_inicial = self.tempo_de_inferencia_inicial
					tempo_de_inferencia_final = time.time()
					tempo_de_inferencia = tempo_de_inferencia_final - tempo_de_inferencia_inicial
					print("Tempo de inferencia: {0:.5f}".format(tempo_de_inferencia))


		@sio.on('register_status')
		def register_status(data):
			print(data)

		@sio.on('disconnect')
		def on_disconnect():
			print('disconnected from server')

		print("[INFO] Connecting to WebSocket server...")

		sio.connect('https://keyapp.ai/socketio')
		n = 0

		while not self.stopped:
			self.tempo_de_inferencia_inicial = time.time()

			# Cropa o frame somente na região aonde as pessoas irão colocar a cabeça (ROI).
			# Região selecionada previamente específica para essa câmera.
			# img[y:y+h, x:x+w]
			img = self.frame[5:720, 350:785]

			try:
				faces = self.net(img, 0)
				for faceRect in faces:
					cvRect = ( int(faceRect.rect.left()), int(faceRect.rect.top()), int(faceRect.rect.right()), int(faceRect.rect.bottom()) )
					facebb = (cvRect[0] -10 , cvRect[1] -20 , cvRect[2] +10, cvRect[3] +20)
					(x,y,w,h) = facebb
					cv2.rectangle(img, (x, y), (w, h), (0, 255, 0), 2)

					d = {
						'frame' : encode_img_base64(img[y:y+h,x:x+w])
					}
					if n%10==0:
						sio.emit('recognize', d)

			
			except:
				pass

			n += 1


			#Descomentar se quiser ver o frame
			#cv2.imshow("Video", img)
			#if cv2.waitKey(1) == ord("q"):
			# 	self.stopped = True


	def stop(self):
		self.sio.disconnect()
		self.stopped = True
