FROM tensorflow/tensorflow:latest-gpu-py3 

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN /bin/bash -c 'apt-get update'
RUN /bin/bash -c 'apt-get -y -o Dpkg::Options::="--force-confmiss" install --reinstall netbase'
RUN /bin/bash -c 'apt-get install -y libsm6 libxext6 libxrender-dev'

ENTRYPOINT ["python","websocket_server.py"]
