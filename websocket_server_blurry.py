import eventlet
import socketio
import cv2
import numpy as np
import pickle
import os

from src.facenet import FaceNet
from src.mtcnn import MTCNN
from src.tools import decode_img_base64, is_frontal_face, find_closest

import time

from keras.models import load_model

blur_detection = load_model("blur_model.h5")

facenet = FaceNet()
mtcnn = MTCNN()

database_imgs_path = 'lfw_cyber'

database = pickle.loads(open(database_imgs_path, "rb").read())

sio = socketio.Server() 

recognition_thresholds = {}
blur_thresholds = {}

people_in = {}

@sio.on('config')
def config(sid, data):
    blur_thresholds[sid] = data['laplacian_threshold']
    recognition_thresholds[sid] = data['recognition_threshold']

    #Client should answer if its in or out

@sio.on('connect')
def connect(sid, environ):
    print('[INFO] Conection estabilished')
 
@sio.on('recognize')
def message(sid, data):
    frame = decode_img_base64(data['frame'])

    bbs, points = mtcnn.detect_faces(frame)
    
    got_scaled = 0
    blurred = 0

    try:
        scaled = mtcnn.get_scaled(frame, bbs[0])
        got_scaled = 1

        image = np.array(scaled,dtype="float")/255.

        image = image.reshape(1,image.shape[0],image.shape[1],image.shape[2])

        pred = blur_detection.predict(image)

        if np.argmax(pred[0]) == 0:
            blurred = 0
            print("not blurry")
        elif np.argmax(pred[0]) == 1:
            blurred = 1
            print("blurry")

    except Exception as e:
        got_scaled = 0
        print(e)

    if got_scaled:

        emb = facenet.run_single(scaled)

        dists = find_closest(emb, database)

        min_distance = max(dists, key=float)  

        index_min_distance = dists.index(min_distance)
        
        identified = database["names"][index_min_distance]

        gray = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)

        orientation = is_frontal_face(bbs, points, frame)
        laplacian_variance = cv2.Laplacian(gray, cv2.CV_64F).var()

        # if orientation == 'Aligned' and min_distance >= recognition_thresholds[sid] and laplacian_variance >= blur_thresholds[sid]:
        if min_distance >= recognition_thresholds[sid] and laplacian_variance >= blur_thresholds[sid]:
            
            resp = {
                'name' : identified,
                'orientation' : orientation,
                'laplacian_variance' : laplacian_variance,
                'min_dist': float(min_distance)
            }
            
            sio.emit('identified', resp, room=sid)
        
        elif laplacian_variance >= blur_thresholds[sid]:

            resp = {
                'name' : 'unknown',
                'orientation' : orientation,
                'laplacian_variance' : laplacian_variance
            }

            sio.emit('identified', resp, room=sid)

@sio.on('register')
def register_face(sid, data):

    global database

    frame = decode_img_base64(data['frame'])

    bbs, points = mtcnn.detect_faces(frame)
    
    got_scaled = 0

    try:
        scaled = mtcnn.get_scaled(frame, bbs[0])
        got_scaled = 1
    except:
        got_scaled = 0

    if got_scaled:

        emb = facenet.run_single(scaled)

        gray = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)

        orientation = is_frontal_face(bbs, points, frame)
        laplacian_variance = cv2.Laplacian(gray, cv2.CV_64F).var()

        if laplacian_variance >= blur_thresholds[sid]:
            print('Registering')

            name = data['name']

            known_encodings = database['encodings']
            names = database['names']
            
            known_encodings.append(emb[0])
            names.append(name)

            data_to_pickle = {"encodings": known_encodings, "names": names}

            f = open(database_imgs_path, "wb")
            f.write(pickle.dumps(data_to_pickle))
            f.close()

            database = pickle.loads(open(database_imgs_path, "rb").read())

            if not os.path.isdir(os.path.join(database_imgs_path, name)):
                
                os.mkdir(os.path.join(database_imgs_path,name))

                cv2.imwrite(os.path.join(database_imgs_path, name, "{}.jpg".format(name)), scaled)

                resp = {
                    'Status' : "Success"
                }
            
            else:
                resp = {
                    'Status' : "Name already registered"
                }

        elif laplacian_variance <= blur_thresholds[sid]:

            resp = {
                'Status' : "Too blury"
            }

        elif orientation != 'Aligned':
            resp = {
                'Status' : "Not aligned"
            }
    else:

        resp = {
            'Status' : "Failed to get face"
        }

    sio.emit('register_status', resp, room=sid)

@sio.on('disconnect')
def disconnect(sid):
    print('disconnect ', sid)

if __name__ == '__main__':

    app = socketio.WSGIApp(sio, static_files={
        '/': {'content_type': 'text/html', 'filename': 'index.html'}
    })

    # eventlet.wsgi.server(eventlet.wrap_ssl(eventlet.listen(('', 5000)), certfile='/home/lucca/crashlabs/faceID/fake.cert', keyfile='/home/lucca/crashlabs/faceID/fake.key', server_side=True), app)

    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)

