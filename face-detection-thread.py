import cv2
from multiprocessing import Process, Lock

# cap = cv2.VideoCapture(0)
#
# faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
#
# faces = []
#
# mutex_buffer = Lock()
#
# mutex = Lock()
#
# frame = None
# frame_copy = None
#
# frame_buffer = []

def captura_frame(frame, faceCascade):
	global faces, frame_buffer
	while(True):
		ok, frame = cap.read()
		print('working')

		mutex_buffer.acquire()
		frame_buffer.append(frame)
		#print(frame_buffer)
		mutex_buffer.release()


		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

		try:
			face = faceCascade.detectMultiScale( gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30) )[0]
			#print(face)
			# if( len(face) >0 ):
			# 	face = face[0]

			mutex.acquire()
			faces.append(face)
			#print(faces)
			mutex.release()
		except:
			pass

def write_frame(faces, frame):
	global frame_copy
	while(True):
		if frame != None:
			frame_copy = frame.copy()
			mutex.acquire()
			for (x, y, w, h) in faces:
				cv2.rectangle(frame_copy, (x, y), (x+w, y+h), (0, 255, 0), 2)
			mutex.release()
			#cv2.imshow("Feed", frame_copy)


if __name__ == '__main__':

	cap = cv2.VideoCapture(0)
	faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")
	faces = []
	mutex_buffer = Lock()
	mutex = Lock()
	frame = None
	frame_copy = None
	frame_buffer = []

	#p1 = Process(target = captura_frame, args = (faces, frame, frame_buffer))
	p1 = Process(target = captura_frame, args = (frame, faceCascade))
	#p2 = Process(target = write_frame, args = (faces, frame))
	p1.start()
	print('oi')
	while(True):
		print("main")
		#print(frame_buffer)
		#global frame_copy
		#frame_copy = frame.copy()

		mutex_buffer.acquire()
		print(frame_buffer)
		if( len(frame_buffer) > 0 ):
			print(frame_buffer)
			frame_copy = frame_buffer.pop()
		mutex_buffer.release()

		if frame_copy != None:
			mutex.acquire()
			for (x, y, w, h) in faces:
				cv2.rectangle(frame_copy, (x, y), (x+w, y+h), (0, 255, 0), 2)
				cv2.imshow("Feed", frame_copy)
			mutex.release()

	#p2.start()
