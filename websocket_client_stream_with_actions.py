import socketio
import cv2
from src.tools import encode_img_base64
import sys
import  json
import requests
#from urllib import request , parse
import time
from videostream import VideoStream

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--rtsp', type=str,
    help='rtsp address', required=True)
args = parser.parse_args()

def reescale(img):
    scale_percent = 70 # percent of original size
    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    dim = (width, height)
    image_reshaped = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
    return image_reshaped

sio = socketio.Client()

if args.rtsp == "0" :
    args.rtsp = 0
elif( args.rtsp == "1" ):
    args.rtsp = 1

#print("rtspsrc location={} latency=0 ! rtph264depay ! h264parse ! omxh264dec ! video/x-raw, width={}, height={},format=(string)RGB ! videoconvert ! appsink".format(args.rtsp, 640, 480))
#gst_str = ("rtspsrc location=rtsp://dev:dev123@192.168.1.5/cam/realmonitor?channel=5&subtype=0 latency=0 ! rtph264depay ! h264parse ! omxh264dec ! video/x-raw, width={}, height={},format=(string)RGB ! videoconvert ! appsink").format(args.rtsp, 640, 480)
gst_str = 'rtspsrc location={} latency=0 ! rtph264depay ! h264parse ! omxh264dec ! video/x-raw, width={}, height={},format=(string)RGB ! videoconvert ! appsink' .format(args.rtsp, 640, 480)

#cap = cv2.VideoCapture(gst_str)
cap = cv2.VideoCapture(args.rtsp)
#cap = cv2.VideoCapture(0)
print("flag")
recognized_name = ""
last_time_detected = time.time()
last_detected = ""

#Actions
def opendoor(prediction=''):
    try:
       # _resp = requests.get("http://aia-webhook.ngrok.io/opendoor?name={}".format(prediction))
        _resp=requests.get("http://192.168.1.160:5000/opendoor")

        print(_resp,_content)
    except IOError:
        _type, value, _traceback = sys.exc_info()
    except Exception as e:
        print(e)


def playaction(prediction=''):
    headers = {
        "Content-Type": "application/json"
    }
    data = json.dumps({
        "name": prediction
    })
    print(headers,data)
    #r = requests.post("http://localhost:7000/projector", headers=headers, data=data)
    r=requests.post("https://keyapp.ngrok.io/projector",headers=headers, data=data)
    print(r)
    if r.status_code != 200:
        print(r.status_code)
        print(r.text)


@sio.on('connect')
def on_connect():
    print('[INFO] Connection successfull')

    config = {
        'recognition_threshold' : 0.65,
        'laplacian_threshold' : 80
    }

    sio.emit('config', config)

@sio.on('connected')
def on_resonse(sid):
    ok, frame = cap.read()

    #print("Connected to front door!")
    # Redimensiona a imagem
    img_reshaped = reescale(frame)

    data = {
        'frame' : encode_img_base64(img_reshaped)
    }

    if ok:
        sio.emit('recognize', data)

@sio.on('identified')
def identified(data):
    global recognized_name
    global last_detected
    global last_time_detected

    identified = data['name']
    orientation = data['orientation']

    if orientation != 'Aligned' and 1==0:
        print(orientation)
    elif identified == 'unknown':
        recognized_name = ""
    else:
        recognized_name = identified
        if last_detected==recognized_name and time.time() - last_time_detected < 30:
            pass
        elif last_detected==recognized_name and time.time() - last_time_detected > 30:
            last_detected=recognized_name
            last_time_detected = time.time()
            opendoor(recognized_name)
            playaction(prediction=recognized_name)

        elif last_detected != recognized_name :
            last_detected=recognized_name
            last_time_detected = time.time()
            opendoor(recognized_name)
            playaction(prediction=recognized_name)


@sio.on('register_status')
def register_status(data):
    print(data)

@sio.on('disconnect')
def on_disconnect():
    print('disconnected from server')

print("[INFO] Connecting to WebSocket server...")

#sio.connect('https://keyservertest.ngrok.io')
sio.connect('https://keyapp.ai/socketio')
n = 0

#_, frame = cap.read()

#region = cv2.selectROI(frame)

#cv2.destroyAllWindows() #Cleanup

# Inicializa o haarcascade
faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

while 1:

    ok, frame = cap.read()

    frame_shape = frame.shape

    #print(frame_shape)

    img_reshaped = reescale(frame)

    print(img_reshaped.shape)


    faces = []

    gray = cv2.cvtColor(img_reshaped, cv2.COLOR_BGR2GRAY)

    try:
        face = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
        )[0]

        faces.append(face)
        for (x, y, w, h) in faces:
            cv2.rectangle(img_reshaped, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(img_reshaped, recognized_name, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)

            d = {
                'frame' : encode_img_base64(img_reshaped[y:y+h,x:x+w])
            }
            if ok and n%10==0:
                sio.emit('recognize', d)

    except:
        recognized_name = ""

    cv2.imshow("Feed", img_reshaped)


    if ord('q') == (cv2.waitKey(1) & 0xFF):
        sio.disconnect()
        break

    n+=1
