from threading import Thread
import cv2

class VideoGet:
	"""
	Class that continuously gets frames from a VideoCapture object
	with a dedicated thread.
	"""

	def __init__(self, src="0"):
		print(src)
		self.grabbed = False
		while( self.grabbed != True):
			self.stream = cv2.VideoCapture(src)
			(self.grabbed, self.frame) = self.stream.read()
			#print(self.grabbed)
		self.stopped = False

	def start(self):
		Thread(target=self.get, args=()).start()
		return self

	def get(self):
		while not self.stopped:
			if not self.grabbed:
				self.stop()
			else:
				(self.grabbed, self.frame) = self.stream.read()
				

	def stop(self):
		self.stopped = True
