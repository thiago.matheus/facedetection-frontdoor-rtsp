from shutil import move, rmtree
import os
import cv2
import random

class Dataset:

    def __init__(self, dataset_path="dataset"):
        """Class for handling faces dataset

        Arguments
        ---------
        dataset_path: str
            Root path of dataset
        """
        self.dataset_path = dataset_path
        self.update_paths_labels("train")
        self.update_paths_labels("test")

    def create_person(self, name, train_or_test):
        """Creates new folder with given persons name in dataset

        Arguments
        ---------
        name: str
            Name of person
        """
        assert not os.path.exists(os.path.join(self.dataset_path,train_or_test, name)), "Person already exists" 
        os.mkdir(os.path.join(self.dataset_path, train_or_test, name))

    def add_img(self, name, img, train_or_test):
        """Adds new image to train dir under given name folder and updates train image_paths and labels

        Arguments
        ---------
        name: str
            Name of person
        img: numpy.ndarray
            Scaled and cropped image of persons face
        """
        assert os.path.exists(os.path.join(self.dataset_path, train_or_test, name)), "Person does not exist in dataset"

        img_count = len(os.listdir(os.path.join(self.dataset_path, train_or_test, name)))

        cv2.imwrite("{}.jpg".format(os.path.join(self.dataset_path, train_or_test, name, str(img_count+1))),img)

        self.update_paths_labels("train")

    def get_img_count(self, name, train_or_test):

        assert os.path.exists(os.path.join(self.dataset_path, train_or_test, name)), "Person does not exist in dataset"
        return len(os.listdir(os.path.join(self.dataset_path, train_or_test, name)))

    def get_dataset(self, d_path, train_or_test):
        """Get ImageClass list with class names and img paths

        Returns
        -------
        dataset: list
            A list containing ImageClass objects for every datset entry
        """
        dataset = []

        path_exp = os.path.expanduser(os.path.join(d_path, train_or_test))
        
        classes = [path for path in os.listdir(path_exp) \
                        if os.path.isdir(os.path.join(path_exp, path))]
        classes.sort()
        nrof_classes = len(classes)
        for i in range(nrof_classes):
            class_name = classes[i]
            facedir = os.path.join(path_exp, class_name)
            image_paths = self.get_image_paths(facedir)
            dataset.append(ImageClass(class_name, image_paths))
    
        return dataset

    def get_image_paths(self, facedir):
        """Gets image paths for given directory 

        Arguments
        ---------
        facedir: str
            Path to directory containing faces

        Returns
        -------
        image_paths: list
            List of all image paths inside facedir
        """
        image_paths = []
        if os.path.isdir(facedir):
            images = os.listdir(facedir)
            image_paths = [os.path.join(facedir,img) for img in images]
        return image_paths

    def get_image_paths_and_labels(self, dataset):
        image_paths_flat = []
        labels_flat = []
        for i in range(len(dataset)):
            image_paths_flat += dataset[i].image_paths
            labels_flat += [i] * len(dataset[i].image_paths)
        return image_paths_flat, labels_flat

    def create_test_set(self):
        """Moves 1 image from each persons train dir to test directory"""

        paths = os.listdir(os.path.join(self.dataset_path, "train"))
        
        for name in paths:
            imgs = os.listdir(os.path.join(self.dataset_path, "train", name))
            
            if not os.path.exists(os.path.join(self.dataset_path, "test", name)):
                os.mkdir(os.path.join(self.dataset_path, "test", name))

            move(os.path.join(self.dataset_path, "train", name, imgs[0]), os.path.join(self.dataset_path, "test", name, imgs[0]))

        self.update_paths_labels("train")
        self.update_paths_labels("test")

    def add_person_test_set(self, name):

        imgs = os.listdir(os.path.join(self.dataset_path, "train", name))

        if not os.path.exists(os.path.join(self.dataset_path, "test", name)):
            os.mkdir(os.path.join(self.dataset_path, "test", name))

        move(os.path.join(self.dataset_path, "train", name, imgs[0]), os.path.join(self.dataset_path,"test", name,imgs[0]))
        
        self.update_paths_labels("train")
        self.update_paths_labels("test")

    def update_paths_labels(self, train_or_test):
        '''Updates paths and labels of dataset'''

        if train_or_test == 'train':
            self.image_paths_train, self.labels_flat_train = self.get_image_paths_and_labels(self.get_dataset(self.dataset_path, train_or_test))
        elif train_or_test == 'test':
            self.image_paths_test, self.labels_flat_test = self.get_image_paths_and_labels(self.get_dataset(self.dataset_path, train_or_test))

    def get_classes(self):
        return [ cls.name.replace('_', ' ') for cls in self.get_dataset(self.dataset_path,"train")]

    
    def remove_person(self, name):
        '''Removes person completely from dataset'''

        rmtree(os.path.join(self.dataset_path, "train", name),  ignore_errors=True)
        rmtree(os.path.join(self.dataset_path, "test", name),  ignore_errors=True)

        self.update_paths_labels("train")
        self.update_paths_labels("test")

    def get_face_image(self, name):
        '''Returns random image from dataset of given name'''

        name = name.replace(' ', '_')
        imgs = os.listdir(os.path.join(self.dataset_path, "train", name))
        img_path = os.path.join(self.dataset_path, "train", name, random.choice(imgs))
        return cv2.imread(img_path)
    
class ImageClass():
    "Stores the paths to images for a given class"
    def __init__(self, name, image_paths):
        self.name = name
        self.image_paths = image_paths
  
    def __str__(self):
        return self.name + ', ' + str(len(self.image_paths)) + ' images'
  
    def __len__(self):
        return len(self.image_paths)