import tensorflow as tf
import numpy as np
import os

class FaceNet:

    def __init__(self, frozen_path="facenet_model/model.pb", gpu_memory_fraction=0.5, gpu_device='0'):
        """FaceNet model class for face encoding.

        Arguments
        ---------
        frozen_path: str
            Path to .pb model. Default="FaceNet/model/model.pb"
        gpu_memory_fraction: float
            Percentage of how much gpu should the model use. If too little model can fail to load. Default=0.5
        gpu_device: str
            Required to set env variable CUDA_VISIBLE_DEVICES so Tensorflow only uses desired gpu. Default='0'
        """

        """Limit visible GPUs"""
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_device

        """Loads tensorflow graph"""
        self.graph = tf.Graph()

        with self.graph.as_default():

            with tf.gfile.FastGFile(frozen_path, "rb") as file:
                
                self.graph_def = tf.GraphDef()

                self.graph_def.ParseFromString(file.read())

                tf.import_graph_def(self.graph_def, name='')

        """Limit GPU memory and create TF Session"""
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
        self.sess = tf.Session(graph=self.graph, config=tf.ConfigProto(gpu_options=gpu_options), )

        """Gets required tensors for inference"""
        self.images_placeholder = self.sess.graph.get_tensor_by_name("input:0")
        self.embeddings = self.sess.graph.get_tensor_by_name("embeddings:0")
        self.phase_train_placeholder = self.sess.graph.get_tensor_by_name("phase_train:0")

    def prewhiten(self,x):
        mean = np.mean(x)
        std = np.std(x)
        std_adj = np.maximum(std, 1.0/np.sqrt(x.size))
        y = np.multiply(np.subtract(x, mean), 1/std_adj)
        return y  

    def run_single(self, img):
        """Finds encoding for given img.

        Arguments
        ---------
        img: numpy.ndarray
            Aligned and cropped face img

        Returns
        -------
        embedding: numpy.ndarray
            512d face encoding
        """
        img = img[:,:,::-1]

        img = self.prewhiten(img)
        
        img = img.reshape(1, img.shape[0], img.shape[1], img.shape[2])

        #Create feed_dict and pass through graph
        feed_dict = { self.images_placeholder:img, self.phase_train_placeholder:False }
        embedding = self.sess.run(self.embeddings, feed_dict=feed_dict)

        return embedding
        