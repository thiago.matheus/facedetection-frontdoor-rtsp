from src.align.detect_face import create_mtcnn, detect_face
import tensorflow as tf
from scipy import misc
import numpy as np
import os

class MTCNN:

    def __init__(self, model_path="src/align/", gpu_memory_fraction=0.3, gpu_device='0'):
        """MTCNN model class for face detection and alignment.

        Arguments
        ---------
        model_path: str
            Path to the three .npy weights for mtcnn. Default="MTCNN/align/"
        gpu_memory_fraction: float
            Percentage of how much gpu should the model use. If too little model can fail to load. Default=0.5
        gpu_device: str
            Required to set env variable CUDA_VISIBLE_DEVICES so Tensorflow only uses desired gpu. Default='0'
        """

        """Limit visible GPUs"""
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_device
        
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_memory_fraction)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

        self.pnet, self.rnet, self.onet = create_mtcnn(sess, model_path)

        self.minsize = 20 # minimum size of face
        self.threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
        self.factor = 0.709 # scale factor

    #TODO There is probably a bug here...
    def detect_faces(self,img, margin=44):
        """Find faces in given img and applies margin for bounding boxes.

        Arguments
        ---------
        img: numpy.ndarray
            Raw image to detect faces
        margin: int
            Extra margin for bounding box

        Returns
        -------
        bounding_boxes: list
            List containing all bounding boxes of detected faces
        """
        img = img[:,:,::-1]

        img_size = np.asarray(img.shape)[0:2]

        bounding_boxes, points = detect_face(img, self.minsize, self.pnet, self.rnet, self.onet, self.threshold, self.factor)
        for det in bounding_boxes:
            det[0] = np.maximum(det[0]-margin/2, 0)
            det[1] = np.maximum(det[1]-margin/2, 0)
            det[2] = np.minimum(det[2]+margin/2, img_size[1])
            det[3] = np.minimum(det[3]+margin/2, img_size[0])

        return bounding_boxes, points

    #TODO There is probably a bug here...
    def get_scaled(self, img, bounding_box, scale_size=160):
        """Get cropped and scaled image

        Arguments
        ---------
        img: numpy.ndarray
            Raw image to crop and scale face
        bounding_box: list
            Bounding box coordinates of face
        scale_size: int
            Size to rescale image

        Returns
        -------
        scaled: numpy.ndarray
            Scaled and cropped face
        """

        cropped = img[int(bounding_box[1]):int(bounding_box[3]),int(bounding_box[0]):int(bounding_box[2]),:]
        scaled = misc.imresize(cropped, (scale_size, scale_size), interp='bilinear')

        return scaled