import argparse
import cv2
from CountsPerSec import CountsPerSec
from ThreadVideoGetFrame import VideoGet
from ThreadVideoProcessConnect import VideoProcessConnect
import socketio
import sys
import  json
import requests
import time

def putIterationsPerSec(frame, iterations_per_sec):
	"""
	Add iterations per second text to lower-left corner of a frame.
	"""

	cv2.putText(frame, "{:.0f} iterations/sec".format(iterations_per_sec),
		(10, 450), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (255, 255, 255))
	return frame


def threadBoth(source="0"):
	"""
	Dedicated thread for grabbing video frames with VideoGet object.
	Dedicated thread for showing, processing video frames and send request with VideoProcessConnect object.
	Main thread serves only to pass frames between VideoGet and
	VideoProcessConnect objects/threads.
	"""
	if( source == "0"):
		source = 0
	#source = "rtsp://cdev:CyB3r@192.168.1.5/cam/realmonitor?channel=5&subtype=0"
	video_getter = VideoGet(source).start()
	video_processer = VideoProcessConnect(video_getter.frame).start()
	cps = CountsPerSec().start()

	while True:
		if video_getter.stopped or video_processer.stopped:
			video_processer.stop()
			video_getter.stop()
			break

		frame = video_getter.frame
		frame = putIterationsPerSec(frame, cps.countsPerSec())
		video_processer.frame = frame
		cps.increment()

def main():

	ap = argparse.ArgumentParser()
	ap.add_argument("--source", "-s", type=str, required=True,
		help="Path to video file or integer representing webcam index"
			+ " (default 0).")
	args = vars(ap.parse_args())

	threadBoth(args["source"])

if __name__ == "__main__":
	main()
