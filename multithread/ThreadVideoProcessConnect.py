from threading import Thread
import socketio
import cv2
from src.tools import encode_img_base64
import sys
import  json
import requests
import time

class VideoProcessConnect:
	"""
	Class that continuously shows a frame using a dedicated thread.
	"""

	def __init__(self, frame=None):
		self.frame = frame
		self.stopped = False
		self.facebb = (0,0,0,0)

		self.cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

		self.recognized_name = ""
		self.last_detected = ""
		self.last_time_detected = time.time()

	def start(self):
		Thread(target=self.show, args=()).start()
		return self

	def show(self):

		self.recognized_name = ""
		self.last_time_detected = time.time()
		self.last_detected = ""

		sio = socketio.Client()

		self.sio= sio

		#Actions
		def opendoor(prediction=''):
			try:
			   # _resp = requests.get("http://aia-webhook.ngrok.io/opendoor?name={}".format(prediction))
				_resp=requests.get("http://192.168.1.160:5000/opendoor")

				print("Resposta request {}".format(_resp))
			except IOError:
				_type, value, _traceback = sys.exc_info()
			except Exception as e:
				print(e)


		def playaction(prediction=''):
			headers = {
				"Content-Type": "application/json"
			}
			data = json.dumps({
				"name": prediction
			})
			print(headers,data)
			#r = requests.post("http://localhost:7000/projector", headers=headers, data=data)
			r=requests.post("https://keyapp.ngrok.io/projector",headers=headers, data=data)
			print(r)
			if r.status_code != 200:
				print(r.status_code)
				print(r.text)


		@sio.on('connect')
		def on_connect():
			print('[INFO] Connection successfull')

			config = {
				'recognition_threshold' : 0.65,
				'laplacian_threshold' : 80
			}

			sio.emit('config', config)

		@sio.on('connected')
		def on_resonse(sid):

			data = {
				'frame' : encode_img_base64(self.frame)
			}

			if ok:
				sio.emit('recognize', data)

		@sio.on('identified')
		def identified(data):

			identified = data['name']
			orientation = data['orientation']

			if orientation != 'Aligned' and 1==0:
				print(orientation)
			elif identified == 'unknown':
				self.recognized_name = ""
			else:
				self.recognized_name = identified
				if self.last_detected==self.recognized_name and time.time() - self.last_time_detected < 30:
					print("Mesma pessoa. Ainda não passou 30 segundos.")
					pass
				elif self.last_detected==self.recognized_name and time.time() - self.last_time_detected > 30 and self.recognized_name != "":
					self.last_detected=self.recognized_name
					self.last_time_detected = time.time()
					opendoor(self.recognized_name)
					playaction(prediction=self.recognized_name)
					print("Mesma pessoa 30 seg depois")

				elif self.last_detected != self.recognized_name and self.recognized_name != "":
					self.last_detected=self.recognized_name
					self.last_time_detected = time.time()
					opendoor(self.recognized_name)
					playaction(prediction=self.recognized_name)
					print("Pessoas diferentes")
				print(time.time() - self.last_time_detected)


		@sio.on('register_status')
		def register_status(data):
			print(data)

		@sio.on('disconnect')
		def on_disconnect():
			print('disconnected from server')

		print("[INFO] Connecting to WebSocket server...")

		sio.connect('https://keyapp.ai/socketio')
		n = 0

		while not self.stopped:
			#for (x, y, w, h) in self.faces:
			gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
			try:
				self.facebb = self.cascade.detectMultiScale( gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30) )[0]
				#faces.append(face)
				(x,y,w,h) = self.facebb
				cv2.rectangle(self.frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
				cv2.putText(self.frame, self.recognized_name, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)

				d = {
					'frame' : encode_img_base64(self.frame[y:y+h,x:x+w])
				}
				# Contador não funciona dentro da thread
				#if ok and n%10==0:
				resp = sio.emit('recognize', d)
				#print("Resposta envio da imagem: {}".format(resp))
			except:
				pass

			n += 1

			# Descomentar se quiser ver o frame
			# cv2.imshow("Video", self.frame)
			# if cv2.waitKey(1) == ord("q"):
			# 	self.stopped = True


	def stop(self):
		self.sio.disconnect()
		self.stopped = True
