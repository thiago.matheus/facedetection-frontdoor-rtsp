import os
import pickle
import time

import cv2
import eventlet
import numpy as np
import socketio
from keras.models import load_model

from src.facenet import FaceNet
from src.mtcnn import MTCNN
from src.tools import decode_img_base64, is_frontal_face, find_closest, cosine_similarity

blur_detection = load_model("blur_model.h5")

facenet = FaceNet()
mtcnn = MTCNN()

database_imgs_path = 'lfw_cyber_multiple'
dataset_path = "dataset"
database = pickle.loads(open(database_imgs_path, "rb").read())

sio = socketio.Server()

recognition_thresholds = {}
blur_thresholds = {}

people_in = {}

c = 0


@sio.on('PING')
def ping(sid, data):
    resp = {'Message': 'PING'}
    sio.emit('PONG', resp, room=sid)


@sio.event
def connect(sid, environ):
    print('[INFO] Conection estabilished')


@sio.event
def disconnect(sid):
    print('disconnect ', sid)


@sio.event
def config(sid, data):
    blur_thresholds[sid] = data['laplacian_threshold']
    recognition_thresholds[sid] = data['recognition_threshold']


@sio.event
def recognize(sid, data):
    session = sio.get_session(sid)
    if 'processing' in session:
        print('Caindo fora...')
        return
    session['processing'] = True
    sio.save_session(sid, session)
    sio.sleep(0.1)

    try:
        global c
        start = time.time()
        frame = decode_img_base64(data['frame'])

        bbs, points = mtcnn.detect_faces(frame)

        got_scaled = 0
        blurred = 0

        try:
            scaled = mtcnn.get_scaled(frame, bbs[0])
            got_scaled = 1

            image = np.array(scaled, dtype="float") / 255.

            image = image.reshape(1, image.shape[0], image.shape[1], image.shape[2])

            # cv2.imshow("dbg cyberama", frame)
            # cv2.imshow("dbg cyberama 2", scaled)
            # cv2.waitKey(1)

            pred = blur_detection.predict(image)

            if np.argmax(pred[0]) == 0:
                blurred = 0
            elif np.argmax(pred[0]) == 1:
                blurred = 1
                print("blurry")

        except Exception as e:
            got_scaled = 0
            blurred = 1
            print(e)

        if got_scaled:
            name = None

            face_emb = facenet.run_single(scaled)

            gray = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)

            orientation = is_frontal_face(bbs, points, frame)

            laplacian_variance = cv2.Laplacian(gray, cv2.CV_64F).var()

            # calculate similarities
            similarities = []

            similarities_names = []

            hsh = {}

            for n, emb in enumerate(database["encodings"]):
                emb = emb.reshape(1, emb.shape[0])
                dist = cosine_similarity(face_emb, emb)[0]
                if dist <= 0.28:
                    similarities.append(dist)
                    name = database["names"][n]
                    similarities_names.append(name)
                    if name in hsh:
                        hsh[name].append(dist)
                    else:
                        hsh[name] = [dist]
            big_key = None
            big_len = 0
            big_sorted = sorted(hsh, key=lambda p: len(hsh[p]), reverse=True)
            for key in big_sorted:
                big_key = key
                big_len = len(hsh[key])
                break
            if big_len <= 10:
                print("FAIL - NAO TEVE MATCHES SUFICIENTES - {}- ({}/10)".format(big_key, big_len))
                resp = {
                    'name':               'unknown',
                    'orientation':        orientation,
                    'laplacian_variance': laplacian_variance
                }
                sio.emit('identified', resp, room=sid)
                return

            if len(hsh) == 0:
                print("FAIL - NAO ACHOU NINGUEM")
                resp = {
                    'name':               'unknown',
                    'orientation':        orientation,
                    'laplacian_variance': laplacian_variance
                }
                sio.emit('identified', resp, room=sid)
                return

            if len(list(hsh.keys())) > 1:

                print("0 -", big_sorted)
                print("1 - NUMBER OF GROUPS len(hsh) {}".format(len(hsh)))
                print("2 - BIGGEST GROUP ", big_key, big_len)

            else:
                print(list(hsh.keys()))
                print("1 - only one BIG GROUP ", big_key, big_len)

            dists = find_closest(face_emb, database)
            min_distance = max(dists, key=float)
            index_min_distance = dists.index(min_distance)

            identified = database["names"][index_min_distance]

            if identified != big_key:
                print("ERROR - OUTPUTS OF BOTH CNN DO NOT MATCH CONFLIT DETECTED {} WITH {}".format(identified, big_key))
                resp = {
                    'name':               'unknown',
                    'orientation':        orientation,
                    'laplacian_variance': laplacian_variance
                }
                sio.emit('identified', resp, room=sid)
                return
            # if orientation == 'Aligned' and min_distance >= recognition_thresholds[sid] and laplacian_variance >= blur_thresholds[sid]:
            if min_distance >= recognition_thresholds[sid] and laplacian_variance >= blur_thresholds[sid] \
                    and orientation != 'Small face':

                resp = {
                    'name':               identified,
                    'orientation':        orientation,
                    'laplacian_variance': laplacian_variance,
                    'min_dist':           float(min_distance)
                }

                print("{} {}".format(identified, float(min_distance)))
                print(
                    " DETECTADO POR SIMILARIDEADE DE COS {} , COS score {} , MIN_distance {}, total time to compute : {} ".format(
                        big_key, min(hsh[big_key]), min_distance, time.time() - start))

                sio.emit('identified', resp, room=sid)
                return
            elif laplacian_variance >= blur_thresholds[sid]:

                resp = {
                    'name':               'unknown',
                    'orientation':        orientation,
                    'laplacian_variance': laplacian_variance
                }

                sio.emit('identified', resp, room=sid)
    finally:
        print("Del...")
        del session['processing']


@sio.event
def register(sid, data):
    global database
    print(data)
    frame = decode_img_base64(data['frame'])

    bbs, points = mtcnn.detect_faces(frame)

    got_scaled = 0

    try:
        scaled = mtcnn.get_scaled(frame, bbs[0])
        got_scaled = 1
    except:
        got_scaled = 0

    if got_scaled:

        emb = facenet.run_single(scaled)

        gray = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)

        orientation = is_frontal_face(bbs, points, frame)
        laplacian_variance = cv2.Laplacian(gray, cv2.CV_64F).var()

        if laplacian_variance >= blur_thresholds[sid]:
            print('Registering')

            name = data['name']

            known_encodings = database['encodings']
            names = database['names']

            known_encodings.append(emb[0])
            names.append(name)

            data_to_pickle = {"encodings": known_encodings, "names": names}

            f = open(database_imgs_path, "wb")
            f.write(pickle.dumps(data_to_pickle))
            f.close()

            database = pickle.loads(open(database_imgs_path, "rb").read())

            if not os.path.isdir(os.path.join(database_imgs_path, name)):

                os.mkdir(os.path.join(database_imgs_path, name))

                cv2.imwrite(os.path.join(database_imgs_path, name, "{}.jpg".format(name)), scaled)

                resp = {
                    'Status': "Success"
                }

            else:
                resp = {
                    'Status': "Name already registered"
                }

        elif laplacian_variance <= blur_thresholds[sid]:

            resp = {
                'Status': "Too blury"
            }

        elif orientation != 'Aligned':
            resp = {
                'Status': "Not aligned"
            }
    else:

        resp = {
            'Status': "Failed to get face"
        }

    sio.emit('register_status', resp, room=sid)


@sio.event
def register_multiple(sid, data):
    global database

    frames = data['frames']

    print("Recebemos {} fotos".format(len(frames)))

    name = data['name']

    if os.path.isdir(os.path.join(dataset_path, database_imgs_path, name)):
        print("Nome ja existe")

        resp = {
            'Status': "Name already registered"
        }

        sio.emit('register_status', resp, room=sid)

        return

    for frame64 in frames:

        frame = decode_img_base64(frame64)

        bbs, points = mtcnn.detect_faces(frame)

        got_scaled = 0

        try:
            scaled = mtcnn.get_scaled(frame, bbs[0])
            got_scaled = 1
        except:
            got_scaled = 0

        if got_scaled:

            emb = facenet.run_single(scaled)

            gray = cv2.cvtColor(scaled, cv2.COLOR_BGR2GRAY)

            orientation = is_frontal_face(bbs, points, frame)
            laplacian_variance = cv2.Laplacian(gray, cv2.CV_64F).var()

            if laplacian_variance >= blur_thresholds[sid]:
                print('Registering')

                known_encodings = database['encodings']
                names = database['names']

                known_encodings.append(emb[0])
                names.append(name)

                data_to_pickle = {"encodings": known_encodings, "names": names}

                f = open(database_imgs_path, "wb")
                f.write(pickle.dumps(data_to_pickle))
                f.close()

                database = pickle.loads(open(database_imgs_path, "rb").read())

                if not os.path.isdir(os.path.join(dataset_path, database_imgs_path, name)):
                    os.mkdir(os.path.join(dataset_path, database_imgs_path, name))

                cv2.imwrite(os.path.join(dataset_path, database_imgs_path, name, "{}{}.jpg".format(name, time.time())),
                            scaled)

                resp = {
                    'Status': "Success"
                }

            elif laplacian_variance <= blur_thresholds[sid]:

                resp = {
                    'Status': "Too blury"
                }

            elif orientation != 'Aligned':
                resp = {
                    'Status': "Not aligned"
                }
        else:

            resp = {
                'Status': "Failed to get face"
            }
    print(resp)
    sio.emit('register_status', resp, room=sid)


if __name__ == '__main__':
    app = socketio.WSGIApp(sio, static_files={
        '/': {'content_type': 'text/html', 'filename': 'index.html'}
    })

    # eventlet.wsgi.server(eventlet.wrap_ssl(eventlet.listen(('', 5000)), certfile='/home/lucca/crashlabs/faceID/nginx-selfsigned.crt', keyfile='/home/lucca/crashlabs/faceID/nginx-selfsigned.key', server_side=True), app)

    eventlet.wsgi.server(eventlet.listen(('', 5000)), app)
