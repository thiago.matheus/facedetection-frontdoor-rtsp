import socketio
import cv2
from src.tools import encode_img_base64

import time

sio = socketio.Client()

# gst_str = ("v4l2src device=/dev/video{} ! video/x-raw, width={}, height={},format=(string)RGB ! videoconvert ! appsink").format(1, 1920, 1080)

cap = cv2.VideoCapture(0)

recognized_name = ""

@sio.on('connect')
def on_connect():
    print('[INFO] Connection successfull')

    config = {
        'recognition_threshold' : 0.65,
        'laplacian_threshold' : 80
    }

    sio.emit('config', config)

@sio.on('connected')
def on_resonse(sid):
    ok, frame = cap.read()

    data = {
        'frame' : encode_img_base64(frame)
    }

    if ok:
        sio.emit('recognize', data)

@sio.on('identified')
def identified(data):
    print("Entrei aki")
    global recognized_name

    identified = data['name']
    orientation = data['orientation']

    if orientation != 'Aligned' and 1==0:
        print(orientation)

    elif identified == 'unknown':
        recognized_name = identified
        print(identified)
    else:
        print('Hello {} {}'.format(identified, data['min_dist']))
        recognized_name = identified

@sio.on('register_status')
def register_status(data):
    print(data)

@sio.on('disconnect')
def on_disconnect():
    print('disconnected from server')

print("[INFO] Connecting to WebSocket server...")

sio.connect('http://localhost:5000')

n = 0

_, frame = cap.read()

region = cv2.selectROI(frame)

cv2.destroyAllWindows() #Cleanup

faceCascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

while 1:

    ok, frame = cap.read()

    frame = frame[int(region[1]):int(region[1]+region[3]), int(region[0]):int(region[0]+region[2])] # crop frame on ROI


    faces = []

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    try:
        face = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
        )[0]

        faces.append(face)
        for (x, y, w, h) in faces:
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(frame, recognized_name, (x,y), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,255), 2)

            d = {
                'frame' : encode_img_base64(frame[y:y+h,x:x+w])
            }
            if ok and n%10==0:
                sio.emit('recognize', d)

    except:
        recognized_name = ""

    cv2.imshow("Feed", frame)

    key = cv2.waitKey(5) & 0xFF

    n+=1

